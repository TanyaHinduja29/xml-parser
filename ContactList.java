import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContactList{
	private ArrayList<Contacts> contacts = new ArrayList<>();
	private XmlForm xmlForm = new XmlForm();
	
	public  void addContact(Contacts c){
		contacts.add(c);
		xmlForm.addToXML(c);
	}
	public void addToList(Contacts c){
		contacts.add(c);
	}
	
	public  void editContact(String name, String phone, String email, String dob, int index){	
		xmlForm.editToXML(contacts.get(index),name,phone,email,dob);
		contacts.get(index).setName(name);
		contacts.get(index).setPhone(phone);
		contacts.get(index).setEmail(email);
		contacts.get(index).setDOB(dob);
	}
	public void removeContact(int index){
		xmlForm.deleteFromXML(contacts.get(index));
		contacts.remove(index);
	}
	public String[][] getData(){
		String[][] dataArray = new String[contacts.size()][];
		for(int i=0;i < contacts.size(); i++){
			dataArray[i] = contacts.get(i).getArray();
		}
		return dataArray;
	}
	public String[] getContactDetails(int index){
		return contacts.get(index).getArray();
	}

	public int getSize(){
		return contacts.size();
	}
	public ArrayList<Contacts> getContacts(){
		return contacts;
	}
	public  Contacts getContact(int index){
		return contacts.get(index);
	}
	public  String getName(int index){
		return contacts.get(index).getName();
	}
	public  String getPhone(int index){
		return contacts.get(index).getPhone();
	}
	public  String getEmail(int index){
		return contacts.get(index).getEmail();
	}
	public  String getDob(int index){
		return contacts.get(index).getDob();
	}

}