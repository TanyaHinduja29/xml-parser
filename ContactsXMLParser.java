import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContactsXMLParser extends JFrame{
	JTabbedPane mainpane;
	
	JButton  edit, delete;
	public ContactList contactList;
	EditContact editContact;
	NewContact newContact;
	DeleteContact deleteContact;
	ContactsTable allcontacts;
	public ContactsXMLParser(){
		Container contentpane = getContentPane();
		contactList =  new ContactList();
		new ParseXML(this);
		editContact = new EditContact(this);
		newContact = new NewContact(this);
		deleteContact = new DeleteContact(this);
		allcontacts = new ContactsTable(this);
		mainpane = new JTabbedPane();
		edit = new JButton("Edit Contact");
		delete = new JButton("Delete Contact");
		mainpane.addTab("All Contacts", allcontacts);
		mainpane.addTab("New", newContact);
		mainpane.addTab("Edit",editContact);
		mainpane.addTab("Delete",deleteContact);
		contentpane.add(mainpane);
		setSize(550,550);
		setVisible(true);
		contactList.getData();
	}
	public static void main(String[] args) {		
		new ContactsXMLParser();
	}
	public void updateTabs(){
		mainpane.setComponentAt(0,new ContactsTable(this));
		mainpane.setComponentAt(1,new NewContact(this));
		mainpane.setComponentAt(2,new EditContact(this));
		mainpane.setComponentAt(3,new DeleteContact(this));
	}

	

}

