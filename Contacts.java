public class Contacts{
	private String name,phone,email,dob;
	Contacts(String name,String phone,String email,String dob){
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.dob = dob;
	}
	public String getName() {
		return this.name;
	}
	public String getPhone() {
		return this.phone;
	}
	public String getEmail() {
		return this.email;
	}
	public String getDob() {
		return this.dob;
	}
	public String[] getArray() {
    String[] dataArray = {name, phone, email, dob};
    return dataArray;
  }
	public void setName(String name) { this.name = name; }
  	public void setPhone(String phone)  { this.phone = phone; }
  	public void setEmail(String email) { this.email = email; }
  	public void setDOB(String dob) { this.dob = dob; }	


  	public String[] contactArray(){
  		String[] contact = {name,phone,email,dob};
  		return contact;
  	}

  	@Override
  public String toString() {
    return "Contact: [" + this.name + " , " + this.phone + " , " + this.email + " , " + this.dob + "] ";
  }
}