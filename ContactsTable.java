import javax.swing.*;
import java.awt.BorderLayout;
public class ContactsTable extends JPanel{
	ContactsXMLParser parentParser;
	JTable contactsTable;
	
	String data[][];
	ContactsTable(ContactsXMLParser parser){
		this.parentParser = parser;
		String colHeads[] = {"Name","Email","Phone No.","Date of Birth"};
		data = parentParser.contactList.getData();
		contactsTable = new JTable(data, colHeads);
		int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
	    int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
	    JScrollPane jsp = new JScrollPane(contactsTable, v, h);;
	    add(jsp);
	}
	


}