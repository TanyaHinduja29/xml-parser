import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class XmlForm{
	File f;
	FileOutputStream fout;
	public XmlForm(){	
		 f= new File("data.xml");	
	}
	public void addToXML(Contacts c){
		try{
			this.fout = new FileOutputStream(f,true);
		}catch(FileNotFoundException e){}
		
		String xmlCode = "\n<Contact>\n"+
			"<name>"+c.getName()+"</name>\n"	+
			"<phoneNumber>"+c.getPhone()+"</phoneNumber>\n"+
			"<email>"+c.getEmail()+"</email>\n"+
			"<DateOfBirth>"+c.getDob()+"</DateOfBirth>\n"+
		"</Contact>\n";	
		try{ 
		   byte b[];
		   b = xmlCode.getBytes();
		   fout.write(b);
		   fout.flush();     
	    }catch(IOException e){}		
	}
	public void editToXML(Contacts c,String name, String phone, String email,String dob){
		String text= "";
		String line="";
		try{
				FileInputStream fis = new FileInputStream(f);  
				BufferedReader in = new BufferedReader(new InputStreamReader(fis)); 
				while(((line = in.readLine())!=null)){
					text+=line;
				}
			}
		catch(FileNotFoundException e){}
		catch(IOException e){}
		try{
			this.fout = new FileOutputStream(f,false);
		}catch(FileNotFoundException e){}

		String replaceString = "\n<Contact>\n"+
			"<name>"+name+"</name>\n"	+
			"<phoneNumber>"+phone+"</phoneNumber>\n"+
			"<email>"+email+"</email>\n"+
			"<DateOfBirth>"+dob+"</DateOfBirth>\n"+
		"</Contact>\n";
		
		String editRegex = "<Contact>\\s*<name>("+c.getName()+")<\\/name>\\s*<phoneNumber>("+c.getPhone()+")<\\/phoneNumber>\\s*<email>("+c.getEmail()+")<\\/email>\\s*<DateOfBirth>("+c.getDob()+")<\\/DateOfBirth>\\s*<\\/Contact>";
		Pattern editPattern = Pattern.compile(editRegex);
		Matcher editmatcher = editPattern.matcher(text);
		System.out.println(editRegex);
		System.out.println(text);

		String editedString = editmatcher.replaceAll(replaceString);
		System.out.println(editedString);
		try{ 
		   byte b[];
		   b = editedString.getBytes();
		   fout.write(b);
		   fout.flush();     
	    }catch(IOException e){}
	}

	public void deleteFromXML(Contacts c){
		String text= "";
		String line="";
		try{
				FileInputStream fis = new FileInputStream(f);  
				BufferedReader in = new BufferedReader(new InputStreamReader(fis)); 
				while(((line = in.readLine())!=null)){
					text+=line;
				}
			}
		catch(FileNotFoundException e){}
		catch(IOException e){}

		try{
			this.fout = new FileOutputStream(f,false);
		}catch(FileNotFoundException e){}

		String deleteRegex = "<Contact>\\s*<name>("+c.getName()+")<\\/name>\\s*<phoneNumber>("+c.getPhone()+")<\\/phoneNumber>\\s*<email>("+c.getEmail()+")<\\/email>\\s*<DateOfBirth>("+c.getDob()+")<\\/DateOfBirth>\\s*<\\/Contact>";
		Pattern deletePattern = Pattern.compile(deleteRegex);
		Matcher deletematcher = deletePattern.matcher(text);
		String deletedString = deletematcher.replaceAll("");

		try{ 
		   byte b[];
		   b = deletedString.getBytes();
		   fout.write(b);
		   fout.flush();     
	    }catch(IOException e){}




	}
}


// Throwable -> Exception
// Exception -> RuntimeException -> all its child are unchecked exception thats why Compiler doesnt give error about them 
// checked Exception -> compiler gives error about it its compulsory to handle or throw the exceptions using throws
// normal Exception class ko jo inherit karte that are checked exceptions


