import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NewContact extends JPanel implements ActionListener{
	JButton addbtn;
	JLabel name,phone,email,dob;
	JTextField nametxt, phonetxt, emailtxt, dobtxt;
	JLabel invalid;
	Panel dobpanel;
	ContactsXMLParser parent;
	NewContact(ContactsXMLParser obj){
		this.parent = obj;
		addbtn = new JButton("Add");
		setLayout(new FlowLayout(FlowLayout.LEFT,100,30));
		name = new JLabel("Name");
		phone = new JLabel("Phone");
		email = new JLabel("Email");
		invalid = new JLabel("");
		dob = new JLabel("Date of Birth");
		dobpanel = new Panel();

		nametxt = new JTextField(15);
		phonetxt = new JTextField(15);
		emailtxt = new JTextField(15);
		dobtxt = new JTextField(15);

		dobpanel.add(dob);
		dobpanel.add(dobtxt);
		dobpanel.setLayout(new FlowLayout(FlowLayout.LEFT,30,10));

		add(name);
		add(nametxt);
		add(phone);
		add(phonetxt);
		add(email);
		add(emailtxt);
		add(dobpanel);
		add(addbtn);
		add(invalid);
		addbtn.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent ae){
		String name = nametxt.getText();
		String phone = phonetxt.getText();
		String email = emailtxt.getText();
		String dob = dobtxt.getText();
		boolean validated = validateData(name,phone,email,dob);
		if(validated){
			parent.contactList.addContact(new Contacts(name,phone,email,dob));
			parent.updateTabs();
			nametxt.setText("");
		    phonetxt.setText("");
		    emailtxt.setText("");
		    dobtxt.setText("");
		}
		else{
			invalid.setText("Please enter Valid Data");
		}

	}
	public static boolean validateData(String name, String phone, String email, String dob){
		String mobileNumberRegex= "[6-9]\\d{9}$";
		Pattern mobileNumberPattern=Pattern.compile(mobileNumberRegex);
		Matcher mobileNumberMatcher=mobileNumberPattern.matcher(phone);

		String emailRegex = "";
		if(mobileNumberMatcher.matches()){
			System.out.println("Valid Mobile number");
			return true;

		}else{
			System.out.println("Invalid Mobile number");
			//invalid.setText("Invalid Phone Number");	
			return false;
		}
	}
}