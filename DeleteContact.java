import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DeleteContact extends JPanel implements ActionListener, ItemListener{
	JComboBox<String> allcontacts;
	JLabel name,phone,email,dob;
	JTextField nametxt, phonetxt, emailtxt, dobtxt;
	JPanel listPanel, fieldPanel;
	JButton deletebtn;
	ContactsXMLParser parentParser;
	DeleteContact(ContactsXMLParser parent){
		this.parentParser = parent;
		setLayout(new BorderLayout());
		allcontacts = new JComboBox<>();
		listPanel = new JPanel();
		listPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		fieldPanel = new JPanel();
		fieldPanel.setLayout(new FlowLayout(FlowLayout.LEFT,20,10));
		name = new JLabel("Name");
		phone = new JLabel("Phone");
		email = new JLabel("Email");
		dob = new JLabel("Date of Birth");
		allcontacts.addItem(" ");
		for(int i=0;i<parentParser.contactList.getSize();i++){
			allcontacts.addItem(parentParser.contactList.getName(i));
		}
		nametxt = new JTextField(15);
		phonetxt = new JTextField(15);
		emailtxt = new JTextField(15);
		dobtxt = new JTextField(15);

		deletebtn = new JButton("Delete");

		listPanel.add(allcontacts);
		fieldPanel.add(name);
		fieldPanel.add(nametxt);
		fieldPanel.add(phone);
		fieldPanel.add(phonetxt);
		fieldPanel.add(email);
		fieldPanel.add(emailtxt);
		fieldPanel.add(dob);
		fieldPanel.add(dobtxt);

		add(listPanel,BorderLayout.NORTH);
		add(fieldPanel,BorderLayout.CENTER);
		add(deletebtn,BorderLayout.SOUTH);

		deletebtn.addActionListener(this);
		allcontacts.addItemListener(this);

	}
	public void actionPerformed(ActionEvent ae){

		parentParser.contactList.removeContact(allcontacts.getSelectedIndex()-1);
		parentParser.updateTabs();
		nametxt.setText("");
		phonetxt.setText("");
		emailtxt.setText("");
		dobtxt.setText("");

	}
	public void itemStateChanged(ItemEvent ie){
		Contacts c = parentParser.contactList.getContact(allcontacts.getSelectedIndex()-1);
		nametxt.setText(c.getName());
		phonetxt.setText(c.getPhone());
		emailtxt.setText(c.getEmail());
		dobtxt.setText(c.getDob());
	}
}