import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class EditContact extends JPanel implements ActionListener, ItemListener{
	JComboBox<String> listofnames;
	JLabel name,phone,email,dob;
	JTextField nametxt, phonetxt, emailtxt, dobtxt;
	Panel listpanel,fieldpanel;
	JButton editbtn;
	ContactsXMLParser parentParser;
	EditContact(ContactsXMLParser obj){
		this.parentParser = obj;
		setLayout(new BorderLayout());
		editbtn = new JButton("Edit");
		listpanel = new Panel();
		fieldpanel = new Panel();
		listofnames = new JComboBox<>();
		listpanel.add(listofnames);
		listpanel.setLayout(new FlowLayout());
		fieldpanel.setLayout(new FlowLayout(FlowLayout.LEFT,20,10));
		listofnames.addItem(" ");
		for(int i=0;i<parentParser.contactList.getSize();i++){
			listofnames.addItem(parentParser.contactList.getName(i));
		}
		name = new JLabel("Name");
		phone = new JLabel("Phone");
		email = new JLabel("Email");
		dob = new JLabel("Date of Birth");

		nametxt = new JTextField(15);
		phonetxt = new JTextField(15);
		emailtxt = new JTextField(15);
		dobtxt = new JTextField(15);

		add(listpanel,BorderLayout.NORTH);
		fieldpanel.add(name);
		fieldpanel.add(nametxt);
		fieldpanel.add(phone);
		fieldpanel.add(phonetxt);
		fieldpanel.add(email);
		fieldpanel.add(emailtxt);
		fieldpanel.add(dob);
		fieldpanel.add(dobtxt);
		add(fieldpanel,BorderLayout.CENTER);
		add(editbtn,BorderLayout.SOUTH);
		editbtn.addActionListener(this);
		listofnames.addItemListener(this);	
	}

	public void actionPerformed(ActionEvent ae){
		String name = nametxt.getText();
		String phone = phonetxt.getText();
		String email = emailtxt.getText();
		String dob = dobtxt.getText();
		parentParser.contactList.editContact(name,phone,email,dob,listofnames.getSelectedIndex()-1);
		parentParser.updateTabs();

	}
	public void itemStateChanged(ItemEvent ie){
		Contacts c = parentParser.contactList.getContact(listofnames.getSelectedIndex()-1);
		nametxt.setText(c.getName());
		phonetxt.setText(c.getPhone());
		emailtxt.setText(c.getEmail());
		dobtxt.setText(c.getDob());
	}

}